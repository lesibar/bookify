# Bookify

## Assumptions: 
- You have the docker engine installed and running
- Commands are linux-based but should work on windows

## Running mysql inside a docker container.
```bash
$ docker run --name=mysql -e MYSQL_DATABASE=bookstore -e MYSQL_ROOT_PASSWORD=changeme -p 3306:3603 mysql:5.7
``` 

MySQL should be available on localhost:3603

# Starting the application

```bash
$ mvn spring-boot:run -Dspring.profiles.active=it
```


# Accessing Swagger 

after successfully starting the application. Navigate to http://localhost:8080/swagger-ui.html to see and try out the available rest endpoints.
