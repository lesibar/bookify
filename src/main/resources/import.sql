TRUNCATE `user`;
TRUNCATE `book_authors`;
TRUNCATE `author_books`;
TRUNCATE `address`;
TRUNCATE `book`;
TRUNCATE `author`;

insert into `user` (id, first_name, last_name, email_address, gender, cell_number) values (1, 'Aubrey', 'Skerme', 'askerme0@elpais.com', 'M', '504 369 1561');
insert into `user` (id, first_name, last_name, email_address, gender, cell_number) values (2, 'Alana', 'Emlin', 'aemlin1@whitehouse.gov', 'F', '406 811 2967');
insert into `user` (id, first_name, last_name, email_address, gender, cell_number) values (3, 'Ernestine', 'Erbe', 'eerbe2@moonfruit.com', 'F', '569 257 0562');
insert into `user` (id, first_name, last_name, email_address, gender, cell_number) values (4, 'Caesar', 'Vickress', 'cvickress3@amazon.de', 'M', '138 456 4358');
insert into `user` (id, first_name, last_name, email_address, gender, cell_number) values (5, 'Bran', 'Vasilov', 'bvasilov4@earthlink.net', 'M', '117 180 3162');

-- Insert Books
insert into `book` (id, date_published, edition, isbn, price, publisher, title) values (1, timestamp(STR_TO_DATE('11/18/2018', '%m/%d/%Y')), '8.7', '329173247-2', 4.0, 'Kohler, Gulgowski and Schroeder', 'The Boogens');
insert into `book` (id, date_published, edition, isbn, price, publisher, title) values (2, timestamp(STR_TO_DATE('1/28/2019', '%m/%d/%Y')), '0.8.0', '309589528-3', 5.0, 'Kreiger-Bins', 'Blackbird');
insert into `book` (id, date_published, edition, isbn, price, publisher, title) values (3, timestamp(STR_TO_DATE('8/9/2018', '%m/%d/%Y')), '0.71', '489050894-5', 6.9, 'Muller and Sons', 'Eagle vs Shark');
insert into `book` (id, date_published, edition, isbn, price, publisher, title) values (4, timestamp(STR_TO_DATE('6/14/2018', '%m/%d/%Y')), '6.0.0', '724328767-0', 9.0, 'Leffler, Berge and Pfannerstill', 'Svampe');
insert into `book` (id, date_published, edition, isbn, price, publisher, title) values (5, timestamp(STR_TO_DATE('7/19/2018', '%m/%d/%Y')), '4.18', '519649246-1', 9.1, 'Thompson-West', 'Sube y Baja');
insert into `book` (id, date_published, edition, isbn, price, publisher, title) values (6, timestamp(STR_TO_DATE('10/24/2018', '%m/%d/%Y')), '5.48', '7069955677', 10.01, 'Kessler-Tremblay', 'Angus');
insert into `book` (id, date_published, edition, isbn, price, publisher, title) values (7, timestamp(STR_TO_DATE('7/20/2018', '%m/%d/%Y')), '4.8', '4888212864', 11.00, 'Heathcote Group', 'Hello Herman');
insert into `book` (id, date_published, edition, isbn, price, publisher, title) values (8, timestamp(STR_TO_DATE('10/26/2018', '%m/%d/%Y')), '0.19', '7441204410', 1.02, 'Ryan, Fisher and Keeling', 'Michael Jackson: Life of a Superstar');
insert into `book` (id, date_published, edition, isbn, price, publisher, title) values (9, timestamp(STR_TO_DATE('5/9/2018', '%m/%d/%Y')), '2.4', '7259615516', 0.99, 'McDermott LLC', 'Deception');
insert into `book` (id, date_published, edition, isbn, price, publisher, title) values (10, timestamp(STR_TO_DATE('5/22/2018', '%m/%d/%Y')), '7.52', '0279962584', 40.78, 'Nolan-Bogan', 'Man of a Thousand Faces');

insert into author (id, first_name, last_name) values (1, 'Bayard', 'Giffin');
insert into author (id, first_name, last_name) values (2, 'Margit', 'Cromer');
insert into author (id, first_name, last_name) values (3, 'Josey', 'Tollet');
insert into author (id, first_name, last_name) values (4, 'Myrtie', 'Creebo');
insert into author (id, first_name, last_name) values (5, 'Deb', 'Valentino');

INSERT INTO `book_authors`(`book_id`,`authors_id`) VALUES (1, 1);
INSERT INTO `book_authors`(`book_id`,`authors_id`) VALUES (2, 2);
INSERT INTO `book_authors`(`book_id`,`authors_id`) VALUES (3, 3);
INSERT INTO `book_authors`(`book_id`,`authors_id`) VALUES (4, 4);
INSERT INTO `book_authors`(`book_id`,`authors_id`) VALUES (5, 5);
INSERT INTO `book_authors`(`book_id`,`authors_id`) VALUES (6, 1);
INSERT INTO `book_authors`(`book_id`,`authors_id`) VALUES (7, 2);
INSERT INTO `book_authors`(`book_id`,`authors_id`) VALUES (8, 3);
INSERT INTO `book_authors`(`book_id`,`authors_id`) VALUES (9, 5);
INSERT INTO `book_authors`(`book_id`,`authors_id`) VALUES (10, 4);