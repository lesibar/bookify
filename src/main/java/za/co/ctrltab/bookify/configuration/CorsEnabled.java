package za.co.ctrltab.bookify.configuration;

import org.springframework.web.bind.annotation.CrossOrigin;

import java.lang.annotation.*;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@CrossOrigin("*")
public @interface CorsEnabled {
}
