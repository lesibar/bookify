package za.co.ctrltab.bookify.web.rest;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import za.co.ctrltab.bookify.domain.Book;
import za.co.ctrltab.bookify.service.BookService;

@RestController(value = "books")
public class BookController {
    private final BookService bookService;

    public BookController(final BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("books/{isbn}")
    public Book getBookService(@PathVariable final String isbn) {
        return bookService.findBookByIsbn(isbn);
    }

    @PutMapping("books")
    public void createBook(@RequestBody  final Book book) {
        bookService.createBook(book);
    }
}
