package za.co.ctrltab.bookify.web.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import za.co.ctrltab.bookify.configuration.CorsEnabled;
import za.co.ctrltab.bookify.domain.User;
import za.co.ctrltab.bookify.service.UserService;

import java.util.List;

@RestController(value= "users")
@RequestMapping(value = "users")
@Slf4j
@CorsEnabled
public class UserController {

    @Autowired
    private UserService userService;

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createUser(@RequestBody final User user) {
        log.info("Creating User [{}]", user.getFirstName());
        userService.createUser(user);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> getAllUsers() {
        List<User> users = userService.getAllUsers();
        log.info("Getting all {} users.", users.size());
        return users;
    }

    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE, params = "emailAddress")
    public User getUserByEmailAddress(@RequestParam final String emailAddress) {
        log.info("Getting user where emailAddress=[{}]", emailAddress);
        return userService.getUserByEmailAddress(emailAddress);
    }

    @PostMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateUser(@PathVariable final Long id, @RequestBody final User user) {
        userService.updateUser(id, user);
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public User getUserById(@PathVariable final Long id) {
        return userService.findUserById(id);
    }

}
