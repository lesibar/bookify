package za.co.ctrltab.bookify.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class Book {
    private String title;
    private List<Author> authors;
    private String edition;
    private String publisher;
    private Date datePublished;
    private String isbn;
    private Double price;
}
