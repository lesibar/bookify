package za.co.ctrltab.bookify.domain;

public enum AppRole {
    ADMIN,
    USER
}
