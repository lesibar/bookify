package za.co.ctrltab.bookify.domain;

public enum AddressType {
    HOME,
    SCHOOL,
    WORK,
    SECONDARY,
    OTHER
}
