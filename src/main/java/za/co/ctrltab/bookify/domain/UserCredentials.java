package za.co.ctrltab.bookify.domain;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserCredentials {
    private String username;
    private String password;
}
