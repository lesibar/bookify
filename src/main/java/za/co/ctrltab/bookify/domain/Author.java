package za.co.ctrltab.bookify.domain;

import lombok.*;

@NoArgsConstructor
@Builder
@AllArgsConstructor
@Getter
@Setter
public class Author {
    private String firstName;
    private String lastName;
}
