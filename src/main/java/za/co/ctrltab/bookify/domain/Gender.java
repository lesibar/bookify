package za.co.ctrltab.bookify.domain;

public enum Gender {
    M("male"),
    F("female"),
    O("other");
    private String gender;

    Gender(final String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }
}
