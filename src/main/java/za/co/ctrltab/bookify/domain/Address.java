package za.co.ctrltab.bookify.domain;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Address {

    private String line1;
    private String line2;
    private String city;
    private String postCode;
    private String province;
    private String country;
    private AddressType addressType;
}
