package za.co.ctrltab.bookify.domain;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class User {
    //`id`, `name`, `lName`, `email`, `cellNum`, `city`, `province`, `address`, `zipCode`, `university`, `course`, `password`
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String cellNumber;
    private Gender gender;
    private UserCredentials userCredentials;
    private List<Address> addresses;
}
