package za.co.ctrltab.bookify.persistence.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import za.co.ctrltab.bookify.domain.Book;

import javax.persistence.*;
import java.util.List;

@Entity(name = "author")
@NoArgsConstructor
@Getter
@Setter
public class AuthorEntity {


    private static final String SEQUENCE_ID = "author_seq";

    @Id
    @SequenceGenerator(name = SEQUENCE_ID, allocationSize = 1)
    @Column(nullable = false, unique = true, name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = SEQUENCE_ID)
    private long id;

    private String firstName;
    private String lastName;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
    private List<BookEntity> books;
}
