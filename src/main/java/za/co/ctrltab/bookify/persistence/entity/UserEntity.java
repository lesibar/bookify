package za.co.ctrltab.bookify.persistence.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import za.co.ctrltab.bookify.domain.Gender;

import javax.persistence.*;

@Entity(name = "user")
@NoArgsConstructor
@Getter
@Setter
public class UserEntity {

    //`id`, `name`, `lName`, `email`, `cellNum`, `city`, `province`, `address`, `zipCode`, `university`, `course`, `password`
    private static final String SEQUENCE_ID = "user_seq";

    @Id
    @SequenceGenerator(name = SEQUENCE_ID, allocationSize = 1)
    @Column(nullable = false, unique = true, name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = SEQUENCE_ID)
    private Long id;

    private String firstName;
    private String lastName;
    private String emailAddress;
    private String cellNumber;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn(name = "user_cred_pk")
    private CredentialsEntity credentials;
}
