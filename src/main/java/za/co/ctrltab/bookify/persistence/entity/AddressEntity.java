package za.co.ctrltab.bookify.persistence.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import za.co.ctrltab.bookify.domain.AddressType;

import javax.persistence.*;

@Entity(name = "address")
@Getter
@Setter
@NoArgsConstructor
public class AddressEntity {

    private static final String SEQUENCE_ID = "address_seq";

    @Id
    @SequenceGenerator(name = SEQUENCE_ID, allocationSize = 1)
    @Column(nullable = false, unique = true, name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = SEQUENCE_ID)
    private long id;

    private String line1;
    private String line2;
    private String city;
    private String postCode;
    private String province;
    private String country;

    @Enumerated(EnumType.STRING)
    private AddressType addressType;
}
