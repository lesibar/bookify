package za.co.ctrltab.bookify.persistence.entity;

import lombok.*;

import javax.persistence.*;

@Entity(name = "credentials")
@NoArgsConstructor
@Getter
@Setter
public class CredentialsEntity {

    private static final String SEQUENCE_ID = "cred_seq";

    @Id
    @SequenceGenerator(name = SEQUENCE_ID, allocationSize = 1)
    @Column(nullable = false, unique = true, name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = SEQUENCE_ID)
    private Long id;

    private String username;
    private String password;
}
