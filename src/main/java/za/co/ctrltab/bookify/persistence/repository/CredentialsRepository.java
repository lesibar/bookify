package za.co.ctrltab.bookify.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.ctrltab.bookify.persistence.entity.CredentialsEntity;

@Repository
public interface CredentialsRepository extends JpaRepository<CredentialsEntity, Long> {
    CredentialsEntity findByUsername(String username);
}
