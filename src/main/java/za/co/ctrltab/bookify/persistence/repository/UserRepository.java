package za.co.ctrltab.bookify.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.ctrltab.bookify.persistence.entity.UserEntity;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByFirstName(String firstName);
    Optional<UserEntity> findByEmailAddress(String emailAddress);
}
