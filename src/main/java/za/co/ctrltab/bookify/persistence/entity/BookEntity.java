package za.co.ctrltab.bookify.persistence.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity(name = "book")
@NoArgsConstructor
@Getter
@Setter
public class BookEntity {

    private static final String SEQUENCE_ID = "book_seq";

    @Id
    @SequenceGenerator(name = SEQUENCE_ID, allocationSize = 1)
    @Column(nullable = false, unique = true, name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = SEQUENCE_ID)
    private long id;

    private String title;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
    private List<AuthorEntity> authors;

    private String edition;
    private String publisher;
    private Date datePublished;
    private String isbn;
    private Double price;
}
