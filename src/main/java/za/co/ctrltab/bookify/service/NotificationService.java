package za.co.ctrltab.bookify.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class NotificationService {

    public void sendEmail(String emailAddress, String subject, String body) {
        log.info("Sending Notification to [{}] Subject=[{}] Body=[{}]", emailAddress, subject, body);
    }

}
