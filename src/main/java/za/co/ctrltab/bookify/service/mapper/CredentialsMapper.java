package za.co.ctrltab.bookify.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import za.co.ctrltab.bookify.domain.UserCredentials;
import za.co.ctrltab.bookify.persistence.entity.CredentialsEntity;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CredentialsMapper {
    CredentialsMapper INSTANCE = Mappers.getMapper(CredentialsMapper.class);

    UserCredentials UserCredentialsToEntity(CredentialsEntity credentialsEntity);
    CredentialsEntity credentialsEntityToCredentials(UserCredentials userCredentials);

}
