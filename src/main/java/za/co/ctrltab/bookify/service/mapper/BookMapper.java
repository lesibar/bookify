package za.co.ctrltab.bookify.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import za.co.ctrltab.bookify.domain.Book;
import za.co.ctrltab.bookify.persistence.entity.AuthorEntity;
import za.co.ctrltab.bookify.persistence.entity.BookEntity;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = AuthorEntity.class)
public interface BookMapper {

    BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

    Book mapEntityToBook(BookEntity bookEntity);
    BookEntity mapBookToEntity(Book book);
}