package za.co.ctrltab.bookify.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import za.co.ctrltab.bookify.domain.Book;
import za.co.ctrltab.bookify.exception.BookNotFoundException;
import za.co.ctrltab.bookify.persistence.repository.BookRepository;
import za.co.ctrltab.bookify.service.mapper.BookMapper;

@Service
@Slf4j
public class BookService {

    private static final String BOOK_TITLE_NOT_FOUND = "Book title not Found";
    private final BookMapper bookMapper;
    private final BookRepository bookRepository;

    public BookService(final BookMapper bookMapper, final BookRepository bookRepository) {
        this.bookMapper = bookMapper;
        this.bookRepository = bookRepository;
    }

    public Book findByBookTitle(final String title) {
        return bookRepository.findByTitle(title).stream().map(bookMapper::mapEntityToBook).findFirst()
                .orElseThrow(() -> new BookNotFoundException(BOOK_TITLE_NOT_FOUND));
    }

    public Book findBookByIsbn(final String isbn) {
        return bookRepository.findByIsbn(isbn).stream().map(bookMapper::mapEntityToBook).findFirst()
                .orElseThrow(() -> new BookNotFoundException(BOOK_TITLE_NOT_FOUND));
    }


    public void createBook(final Book book) {
        bookRepository.save(bookMapper.mapBookToEntity(book));
    }
}
