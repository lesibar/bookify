package za.co.ctrltab.bookify.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import za.co.ctrltab.bookify.domain.User;
import za.co.ctrltab.bookify.domain.UserCredentials;
import za.co.ctrltab.bookify.persistence.entity.CredentialsEntity;
import za.co.ctrltab.bookify.persistence.entity.UserEntity;

@Mapper(componentModel = "spring", unmappedTargetPolicy= ReportingPolicy.IGNORE, uses = CredentialsEntity.class)
public interface UserMapper {

    User map(UserEntity userEntity);
    UserEntity map(User user);

}
