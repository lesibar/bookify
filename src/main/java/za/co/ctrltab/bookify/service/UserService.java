package za.co.ctrltab.bookify.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import za.co.ctrltab.bookify.domain.User;
import za.co.ctrltab.bookify.exception.UserNotFoundException;
import za.co.ctrltab.bookify.persistence.entity.UserEntity;
import za.co.ctrltab.bookify.persistence.repository.UserRepository;
import za.co.ctrltab.bookify.service.mapper.UserMapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserService {

    public static final String USER_NOT_FOUND = "User not found.";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public List<User> getAllUsers() {
        return userRepository.findAll().stream().map(userEntity ->
                userMapper.map(userEntity)).collect(Collectors.toList());
    }

    public void createUser(final User user) {
        UserEntity userEntity = userMapper.map(user);
        userEntity.getCredentials().setPassword(bCryptPasswordEncoder.encode(user.getUserCredentials().getPassword()));
        final UserEntity save = userRepository.save(userEntity);
        log.info("Saved encrypted Password: [{}]", save.getCredentials().getPassword());
    }

    public User getUserByEmailAddress(final String emailAddress) {
        log.info("Finding user where EmailAddress=[{}]", emailAddress);
        Optional<UserEntity> userEntity = userRepository.findByEmailAddress(emailAddress);
        if(userEntity.isPresent()) {
            return userMapper.map(userEntity.get());
        } else {
            throw new UserNotFoundException(USER_NOT_FOUND);
        }
    }

    public void updateUser(final Long id, final User user) {
        final Optional<UserEntity> userEntityOptional = userRepository.findById(id);

        if (userEntityOptional.isPresent()) {
            final UserEntity userEntity = userEntityOptional.get();
            userEntity.setCellNumber(user.getCellNumber());
            userEntity.setEmailAddress(user.getEmailAddress());
            userEntity.setFirstName(user.getFirstName());
            userEntity.setLastName(user.getLastName());
            userEntity.setGender(user.getGender());
            userRepository.save(userEntity);
        } else {
            throw new UserNotFoundException(USER_NOT_FOUND);
        }
    }

    public User findUserById(final Long id) {
        final Optional<UserEntity> userEntity = userRepository.findById(id);

        if (userEntity.isPresent()) {
            return userMapper.map(userEntity.get());
        } else {
            throw new UserNotFoundException(USER_NOT_FOUND);
        }
    }
}
